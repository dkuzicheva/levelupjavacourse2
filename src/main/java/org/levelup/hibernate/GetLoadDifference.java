package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.config.HibernateConfiguration;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.repository.FacultyRepository;
import org.levelup.hibernate.repository.HibernateFacultyRepository;

public class GetLoadDifference {


    public static void main(String[] args) {

        SessionFactory factory = HibernateConfiguration.getFactory();

        FacultyRepository repository = new HibernateFacultyRepository(factory);
       // FacultyEntity faculty = repository.getById(42343);
       // System.out.println("getById:" + faculty.getName());


        FacultyEntity faculty1 = repository.loadById(334);
        System.out.println("getById:" + faculty1.getFacultyId());

        factory.close();



    }


}
