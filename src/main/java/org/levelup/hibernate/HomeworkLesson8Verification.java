package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.config.HibernateConfiguration;
import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.StudentEntity;
import org.levelup.hibernate.domain.StudentGroupEntity;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.*;

import java.sql.Date;

public class HomeworkLesson8Verification {

    public static void main(String[] args) {

        SessionFactory factory = HibernateConfiguration.getFactory();

        DepartmentRepository departmentRepository = new HibernateDepartmentRepository(factory);
        StudentGroupRepository studentGroupRepository = new HibernateStudentGroupRepository(factory);
        StudentRepository studentRepository = new HibernateStudentRepository(factory);
        FacultyRepository facultyRepository = new HibernateFacultyRepository(factory);


//Получение департамента по ID и создание нового департамента в связке с факультетом


        DepartmentEntity departmentEntity = departmentRepository.getDepartmentById(1);
        System.out.println("Название полученного департамента: " + departmentEntity.getName());


        DepartmentEntity departmentEntity1 = departmentRepository
                .createDepartment(5, 4,  "Кафедра систем коммутации");

        System.out.println("ID Кафедры систем связи: " + departmentEntity1.getId());


//Создание новой студенческой группы в связке с департаментом и получение по группы ID


        StudentGroupEntity studentGroupEntity = studentGroupRepository.getStudentGroupById("2");
        System.out.println("Group_key группы: " + studentGroupEntity.getGroupKey()
                + "; Группа принадлежит департаменту: " + studentGroupEntity.getGroupDepartmentId().getId());



        StudentGroupEntity studentGroupEntity1 = studentGroupRepository
                .createStudentGroup("3",2);

        System.out.println("Group_key группы: " + studentGroupEntity1.getGroupKey()
                + "; Группа принадлежит департаменту: " + studentGroupEntity1.getGroupDepartmentId().getId());


//Создание нового студента в связке с его группой и получение по studentCard

        StudentEntity studentEntity = studentRepository.getStudentById(1234);
        System.out.println("Имя студента: " + studentEntity.getFirstName() + " " +studentEntity.getLastName() + " "
                + "Дата рождения: " + studentEntity.getBirthday());


        Date date = Date.valueOf("1998-11-01");
        StudentEntity studentEntity1 = studentRepository
                .createStudent(12345, "Petrov", "Petr", date, "3");

        System.out.println("Имя созданного студента: " + studentEntity1.getFirstName() + " " +studentEntity1.getLastName() + " "
                + "Дата рождения: " + studentEntity1.getBirthday());


    }

}
