package org.levelup.hibernate.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "department")
public class DepartmentEntity {


    @Id
   // @OneToMany(mappedBy = "groupDepartmentId")
    private Integer id;  //-----> departmentId не генерируется из секвенса

    private String name;



    @ManyToOne()
    @JoinColumn(name = "faculty_id", nullable = false)
    private FacultyEntity departmentFacultyId;


    @OneToMany(mappedBy = "groupDepartmentId")
    private List<StudentGroupEntity> studentGroup;



    public DepartmentEntity(Integer id, String name){
        this.id = id;
        this.name = name;

    }








}
