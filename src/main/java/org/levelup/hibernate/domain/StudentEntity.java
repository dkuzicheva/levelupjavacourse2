package org.levelup.hibernate.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;


//TODO make scheme of table once again

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "student")
public class StudentEntity {



    @Id
    @Column(name = "student_card")
    private Integer studentCard;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(nullable = false)
    private Date birthday;


    @ManyToOne()
    @JoinColumn(name = "group_key", nullable = false)
    private StudentGroupEntity studentGroupKey;


    public StudentEntity (Integer studentCard, String lastName, String firstName, Date birthday){
        this.studentCard = studentCard;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthday = birthday;
    }




}
