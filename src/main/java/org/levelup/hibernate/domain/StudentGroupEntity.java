package org.levelup.hibernate.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "student_group")
public class StudentGroupEntity {


    @Id
    @Column(name = "group_key", nullable = false, unique = true)
    private String groupKey;


    @ManyToOne()
    @JoinColumn(name = "department_id", nullable = false)
    private DepartmentEntity groupDepartmentId;


    @OneToMany(mappedBy = "studentGroupKey")
    private List<StudentEntity> students;


    public StudentGroupEntity(String groupKey){
        this.groupKey = groupKey;
    }



}
