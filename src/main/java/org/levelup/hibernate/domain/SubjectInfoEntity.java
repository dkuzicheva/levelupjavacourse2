package org.levelup.hibernate.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "subject_info")
@NoArgsConstructor
@AllArgsConstructor
public class SubjectInfoEntity {

@Id
@Column(name = "subject_id")
private Integer subjectId;

private String description;

@Column(name = "room_number")
private Integer roomNumber;


@OneToOne(fetch= FetchType.LAZY)
@PrimaryKeyJoinColumn
private SubjectEntity subject;






}
