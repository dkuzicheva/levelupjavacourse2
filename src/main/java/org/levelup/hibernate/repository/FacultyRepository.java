package org.levelup.hibernate.repository;



// 1 layer --- Controllers
// 2 layer --- Services
// 3 layer --- DAO (repositories)

//Interface DAO
//Data Access Object (DAO)


// classical dao:
// - CRUD operations: create, read (readAll, readById), update, delete
// - no methods like findByField, checkSomething

// Repository -- тот же DAO + поисковые методы
// Фактически в разговорной речи проекта DAO то же самое ,что Repository


import org.levelup.hibernate.domain.FacultyEntity;

public interface FacultyRepository {

 FacultyEntity createFaculty (Integer universityId, Integer facultyId, String name);
 FacultyEntity getById (Integer facultyId);
 FacultyEntity loadById (Integer facultyId);





}
