package org.levelup.hibernate.repository;

import org.levelup.hibernate.domain.StudentEntity;
import org.levelup.hibernate.domain.StudentGroupEntity;

import java.util.Date;

public interface StudentGroupRepository {


    StudentGroupEntity createStudentGroup (String groupKey, Integer groupDepartmentId);
    StudentGroupEntity getStudentGroupById (String groupKey);



}
