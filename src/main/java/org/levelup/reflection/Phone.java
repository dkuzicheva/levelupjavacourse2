package org.levelup.reflection;

public class Phone {

    private String model;
    public int ram;
    private double cpu;


    private Phone (double cpu){
        this.cpu = cpu;
    }

    public Phone (String model, int ram, double cpu){
        this.cpu = cpu;
        this.model = model;
        this.ram = ram;
    }

    public Phone(){}



    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public double getCpu() {
        return cpu;
    }

    public void setCpu(double cpu) {
        this.cpu = cpu;
    }

}
