package org.levelup.reflection.homeworklesson3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppService {


    public List<String> findClasses(String packageName){

        List<String> names = new ArrayList<>();

        if (!packageName.contains("org.levelup.reflection")) {
            System.out.println("Задано неправильное имя пакета");
        } else {

            File[] files;

            packageName = "C:/Users/dkuziche/Documents/DOCs/Courses/java_course2_kuzicheva/src/main/java/" + packageName.replace(".", "/");
            File f = new File(packageName);
            files = f.listFiles();

            for (File pathname : files) {
                if (pathname.getName().contains(".java")) {
                    names.add(pathname.getName().substring(0, pathname.getName().lastIndexOf(".")));
                }

            }

//-------------------------------------------------------------
// using Thread.currentThread.getContextClassLoader() ---- ????
//-------------------------------------------------------------
//            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//            URL packageURL;
//
//
//            packageName = packageName.replace(".", "/");
//            packageURL = classLoader.getResource(packageName);
//
//
//            URI uri = new URI(packageURL.toString());
//            File folder = new File(uri.getPath());
//            File[] files = folder.listFiles();
//            String entryName;
//            for(File actual: files){
//                entryName = actual.getName();
//                if (entryName.contains(".class")){
//                    names.add(entryName);
//                }
//            }


        }

        return names;
    }

    public void createObjForClassesWithoutAnnotation(String packageName, List<String> classNames) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        for (String name: classNames) {
            Class clazz = Class.forName(packageName + "." + name);
            ReflectionClass annotation = (ReflectionClass) clazz.getAnnotation(ReflectionClass.class);
            if (annotation == null) {
                Object obj = clazz.newInstance();
                System.out.println(obj.toString());
            }
        }

    }


}
