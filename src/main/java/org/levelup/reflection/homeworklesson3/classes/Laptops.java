package org.levelup.reflection.homeworklesson3.classes;

public class Laptops {


    private String firm;
    private String model;
    private int memory;


    public Laptops(){
        this.firm = "Lenovo";
        this.model = "T580";
        this.memory = 16;
    }


    @Override
    public String toString() {
        return "Laptops{" +
                "firm='" + firm + '\'' +
                ", model='" + model + '\'' +
                ", memory=" + memory +
                '}';
    }
}
