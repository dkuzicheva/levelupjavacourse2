package org.levelup.thread;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

public class CounterApp {

    public static void main(String[] args) throws InterruptedException {


        Counter counter = new Counter();

        Thread t1 = new Thread(new CounterWorker(counter),"t1");
        Thread t2 = new Thread(new CounterWorker(counter),"t2");
        Thread t3 = new Thread(new CounterWorker(counter),"t3");


        t1.start();
        t2.start();
        t3.start();


        t1.join();
        t2.join();
        t3.join();

        System.out.println("Total count: "+ counter.getValue());
    }




    static class Counter{


        private int value;

//        public synchronized void increment(){
//            value++;
//        }

        public void increment(){

            synchronized (this){
                value++;
            }

        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }


    }


    @AllArgsConstructor
    //@SneakyThrows
    static class CounterWorker implements Runnable{

        private Counter counter;


        @SneakyThrows
        @Override
        public void run() {

            for (int i =0 ; i<20;i++){
                counter.increment();
                System.out.println(Thread.currentThread().getName()+ " " + counter.getValue());
                Thread.sleep(100);

            }

        }
    }
}
