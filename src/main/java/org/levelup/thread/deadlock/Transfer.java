package org.levelup.thread.deadlock;

import com.sun.org.apache.xerces.internal.xs.XSModel;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Transfer implements Runnable{


    private final Account first;
    private final Account second;
    private final TransferService service;
    private final double amount;


    @Override
    public void run() {
      service.transferMoney(first,second, amount);
    }
}
