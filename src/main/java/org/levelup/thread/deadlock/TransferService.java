package org.levelup.thread.deadlock;

public class TransferService {


//To transfer money from one account to another
    public void transferMoney(Account a, Account b, double amount){


        Account first = a.getAccountId().compareTo(b.getAccountId()) >=0 ? a:b;
        Account second = first == a ? b:a;

        synchronized (first){
            synchronized (second){
                a.setAmount(a.getAmount()-amount);
                b.setAmount(b.getAmount()+amount);
            }
        }


    }
}
