package org.levelup.thread.queue;

import java.util.LinkedList;

public class BlockingQueue implements Queue{

    private final LinkedList<Runnable> tasks;
    private final int capacity;


    private final Object fullQueue = new Object();
    private final Object emptyQueue = new Object();


    BlockingQueue(int capacity){
        this.tasks = new LinkedList<>();
        this.capacity = capacity;
    }

    @Override
    public void add(Runnable runnable) throws InterruptedException {

        synchronized (fullQueue){
            while (tasks.size() == capacity){
                // wait()
                   fullQueue.wait();

            }
        }

        synchronized (emptyQueue){
            tasks.addLast(runnable);
            emptyQueue.notifyAll();  // разбудили потоки ,которые ждали очередь
        }



    }

    @Override
    public Runnable take() throws InterruptedException {

        synchronized (emptyQueue){
            while (tasks.isEmpty()){
                emptyQueue.wait(); // усыпили потоки, если очередь пуста
            }
        }

        synchronized (fullQueue){
            Runnable task = tasks.poll();
            fullQueue.notifyAll();
            return task;
        }

    }


}
