package org.levelup.thread.queue;

public interface Queue {

    //добавить в конец очереди
  void add(Runnable runnable) throws InterruptedException;


    //взять с конца очереди
  Runnable take() throws InterruptedException;





}
