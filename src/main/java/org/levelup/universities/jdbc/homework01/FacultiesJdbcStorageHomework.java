package org.levelup.universities.jdbc.homework01;

import java.sql.*;

public class FacultiesJdbcStorageHomework {


    public JdbcServiceHomework jdbcServiceHomework;

    public FacultiesJdbcStorageHomework(){
        this.jdbcServiceHomework = new JdbcServiceHomework();
    }


    public void createFaculty(String name, int universityId){


        try (Connection connection = jdbcServiceHomework.openConnection()){

            //Statement , PreparedStatement, CallableStatement

            PreparedStatement statement = connection.prepareStatement("insert into faculty (name, university_id) values (?, ?)");

            statement.setString(1,name);
            statement.setInt(2,universityId);

            int rowsAffected = statement.executeUpdate();
            System.out.println("Rows affected:" + rowsAffected);

        }catch(SQLException exp){
            System.out.println("Could not open connection"+exp.getMessage());
            throw new RuntimeException();
        }


    }

    public void displayFaculties() {
        try (Connection connection = jdbcServiceHomework.openConnection()) {

            //Statement , PreparedStatement, CallableStatement

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select * from faculty");
            while (result.next()) {
                //result -- указатель на текущую строку в цикле
                int id = result.getInt(1);
                String name = result.getString("name");
                int universityId = result.getInt(3);

                System.out.println(id + " " + name + " " + universityId);

            }


        } catch (SQLException exp) {
            System.out.println("Couldnot open connection" + exp.getMessage());
            throw new RuntimeException();
        }
    }



    public void findBySql(String sql,Object...args){

            try (Connection connection = jdbcServiceHomework.openConnection()) {

                //Statement , PreparedStatement, CallableStatement

                PreparedStatement statement = connection.prepareStatement(sql);

                int parameterIndex = 1;

                for (Object argument: args) {
                    statement.setObject(parameterIndex++, argument);
                }
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    //result -- указатель на текущую строку в цикле
                    int id = result.getInt(1);
                    String name = result.getString("name");
                    int universityId = result.getInt(3);

                    System.out.println(id + " " + name + " " + universityId);

                }

            } catch (SQLException exp) {
                System.out.println("Couldnot open connection" + exp.getMessage());
                throw new RuntimeException();
            }
        }



    }





